//
//  Urls.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import Foundation







enum Urls {
    //TODO:- make in-accissible properties private
    private static let baseUrl = "http://api.edmunds.com/api/vehicle/v3/"
    private static let apiKey: String = "2ep93tpnyh6p5hgaxmp6pasq"
    
    // MARK:- Auth Cases
    case makes(page:Int)
   
    case models(page:Int,makeNiceName:String)

    
    
    var url: String {
        
        switch self {
        
        case .makes(let page): return Urls.baseUrl + "makes?api_key=\(Urls.apiKey)&pageNum=\(page)&pageSize=20&sortby=name:ASC&fields=id,name,niceName"
        case .models(let page,let makeNiceName): return Urls.baseUrl + "models?makeNiceName=\(makeNiceName)&api_key=\(Urls.apiKey)&pageNum=\(page)&pageSize=20"
     
        }
    }
}
