//
//  AppCoordinator.swift
//  MVVMRxSwift
//
//  Created by a7med on 29/12/2021.
//

import UIKit
protocol Coordinator:class {
    var childCoordinators :  [Coordinator] { get }
    
    func start()
}

class AppCoordinator:Coordinator {
    private (set) var childCoordinators: [Coordinator] = []

    private var window: UIWindow
    
    init(window:UIWindow) {
        self.window = window
    }
    func start()  {
       
        
        let navigationController = UINavigationController()
        let makesListCoordinator = MakesListCoordinator(navigationController: navigationController)
        childCoordinators.append(makesListCoordinator)
        makesListCoordinator.start()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
    }
}
