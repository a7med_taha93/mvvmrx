//
//  ModelsListCoordinator.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import UIKit


final class ModelsListCoordinator:Coordinator{
    private (set) var childCoordinators: [Coordinator] = []
    private let  navigationController: UINavigationController
    var parentCoordinator :  MakesListCoordinator?
    private var makeNiceName:String
    init(navigationController:UINavigationController,makeNiceName:String) {
        self.navigationController = navigationController
        self.makeNiceName = makeNiceName
    }
    func start() {
        
        let viewModel = ModelsListViewModel(coordinator: self, makeNiceName: makeNiceName)
        let modelsVC:ModelsVC = ModelsVC(viewModel: viewModel)
        navigationController.pushViewController(modelsVC, animated: true)
        
        
        
    }
    
    func didFinish()  {
        parentCoordinator?.childDidFinish(self)
        
    }
    func startDidSelectModelEvent()  {
        print("model did selected")
    }
   
  
    func childDidFinish(_ childCoordinator: Coordinator)  {
           if let index  = childCoordinators.firstIndex(where: { coordinator -> Bool in
               return childCoordinator === coordinator
           }){
               childCoordinators.remove(at: index)
           }
       }
    
    
}
