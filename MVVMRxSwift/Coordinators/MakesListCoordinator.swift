//
//  MakesListCoordinator.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import UIKit

final class MakesListCoordinator:Coordinator{
  private (set)  var childCoordinators: [Coordinator] = []
    private var navigationController: UINavigationController
    init(navigationController:UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let makesVC  = MakesVC(viewModel: MakesListViewModel(coordinator: self))
        navigationController.setViewControllers([makesVC], animated: false)
    }
    
    func startDidSelectMakeEvent(with makeNiceName:String)  {
        let modelsListCoordinator = ModelsListCoordinator(navigationController:navigationController, makeNiceName: makeNiceName)
        modelsListCoordinator.parentCoordinator = self
        childCoordinators.append(modelsListCoordinator)
        modelsListCoordinator.start()
    }
    
    func childDidFinish(_ childCoordinator: Coordinator)  {
        if let index  = childCoordinators.firstIndex(where: { coordinator -> Bool in
            return childCoordinator === coordinator
        }){
            childCoordinators.remove(at: index)
        }
    }
    
}
