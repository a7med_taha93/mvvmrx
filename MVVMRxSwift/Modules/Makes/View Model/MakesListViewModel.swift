//
//  MakesListViewModel.swift
//  MVVMRxSwift
//
//  Created by a7med on 25/02/2022.
//

import Foundation
import RxSwift

final class MakesListViewModel{
    
    let title = "Makes"
    var page = 1
    var coordinator : MakesListCoordinator?

    private let makesService: MakesServiceProtocal
    
    init(makesService: MakesServiceProtocal = MakesService(),coordinator:MakesListCoordinator) {
        self.makesService = makesService
        self.coordinator = coordinator
    }
    
    
    func fetchMakeViewModels() -> Observable<[MakeViewModel]> {
        return makesService.fetchMakes(page: page).map{$0.map{ MakeViewModel(make: $0) }}
    }
    
    func fetchMoreMakeViewModels() -> Observable<[MakeViewModel]> {
        page += 1
        return makesService.fetchMakes(page: page).map{$0.map{ MakeViewModel(make: $0) }}
    }
    
    func didSelectMake(_ makeViewModel: MakeViewModel)  {
        guard let niceName = makeViewModel.niceName else { return  }
        coordinator?.startDidSelectMakeEvent(with: niceName)
    }
    
    
}
