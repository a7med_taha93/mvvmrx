//
//  MakesService.swift
//  MVVMRxSwift
//
//  Created by a7med on 25/02/2022.
//

import Foundation
import RxSwift

protocol MakesServiceProtocal {
    func fetchMakes(page: Int) -> Observable<[Make]>
}

class MakesService:MakesServiceProtocal{
    
    func fetchMakes(page: Int = 1) -> Observable<[Make]> {
        return Observable.create { (observer) -> Disposable in
            guard let url = URL(string: Urls.makes(page: page).url) else{ return Disposables.create()}

            let task  = URLSession.shared.dataTask(with: url) { (data, urlresponse, error) in
                if let error = error {
                    observer.onError(error)
                }
                
                if let data = data { 
                    
                    do {
                        let makes_model = try JSONDecoder().decode(Makes_Model.self, from: data)
                        let makes = makes_model.makes ?? []
                        observer.onNext(makes)
                    } catch  {
                        observer.onError(error)
                    }
                }
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
        
    }
    
    
}
