//
//  MakeViewModel.swift
//  MVVMRxSwift
//
//  Created by a7med on 25/02/2022.
//

import Foundation


struct MakeViewModel {
    
    private let make : Make
    
    var displayText: String?{
        return make.name
    }
    
    var niceName: String?{
        return make.niceName
    }
    

    
    init(make:Make) {
        self.make = make
    }
}
