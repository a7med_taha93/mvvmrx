//
//  MakeCell.swift
//  MVVMRxSwift
//
//  Created by a7med on 25/02/2022.
//

import UIKit

class MakeCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
