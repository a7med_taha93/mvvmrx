import Foundation

// MARK: - Categories
struct Categories:Codable {
    let vehicleStyle, market: [String]?
}
