import Foundation

// MARK: - Result
struct Model:Codable {
    let id: String?
    let makeID: Int?
    let name, niceName, adTargetID, niceID: String?
    let modelLinkCode: String?
    let modelYears: [ModelYear]?
    let categories: Categories?
    let categoryValues: [String]?
}
