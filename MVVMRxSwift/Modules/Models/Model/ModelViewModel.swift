//
//  ModelViewModel.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import Foundation


struct ModelViewModel {
    
    private let model : Model
    
    var displayText: String?{
        return model.name
    }
    
    var niceName: String?{
        return model.niceName
    }
    

    
    init(model:Model) {
        self.model = model
    }
}
