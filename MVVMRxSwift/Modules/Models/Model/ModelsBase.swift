import Foundation

// MARK: - ModelsBase
struct ModelsBase:Codable {
    let totalNumber, totalPages: Int?
    let results: [Model]?
}
