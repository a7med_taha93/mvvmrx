import Foundation

// MARK: - ModelYear
struct ModelYear:Codable {
    let id: Int?
    let name: String?
    let year: Int?
    let href: String?
}
