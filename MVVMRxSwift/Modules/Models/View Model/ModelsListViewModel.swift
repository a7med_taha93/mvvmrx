//
//  ModelsListViewModel.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import Foundation
import RxSwift

final class ModelsListViewModel{
    
    let title = "Models"
    var page = 1
    private var makeNiceName: String
    private let modelsService: ModelsServiceProtocal
    private var coordinator : ModelsListCoordinator?

    init(modelsService: ModelsServiceProtocal = ModelsService(),coordinator:ModelsListCoordinator,makeNiceName: String) {
        self.modelsService = modelsService
        self.coordinator = coordinator
        self.makeNiceName = makeNiceName
    }
    
    
    func fetchModelViewModels() -> Observable<[ModelViewModel]> {
        return modelsService.fetchModels(page:page,makeNiceName: makeNiceName).map{$0.map{ ModelViewModel(model: $0) }}
    }
    
    func didSelectModel(_ makeViewModel: MakeViewModel)  {
         coordinator?.startDidSelectModelEvent()
    }
    
    
}
