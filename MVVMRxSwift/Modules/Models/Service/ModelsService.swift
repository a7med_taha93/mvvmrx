//
//  ModelsService.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import Foundation
import RxSwift

protocol ModelsServiceProtocal {
    func fetchModels(page: Int,makeNiceName: String) -> Observable<[Model]>
}

class ModelsService:ModelsServiceProtocal{
   
    
    func fetchModels(page: Int = 1,makeNiceName: String) -> Observable<[Model]> {
        return Observable.create { (observer) -> Disposable in
            guard let url = URL(string: Urls.models(page: page, makeNiceName: makeNiceName).url) else{ return Disposables.create()}

            let task  = URLSession.shared.dataTask(with: url) { (data, urlresponse, error) in
                if let error = error {
                    observer.onError(error)
                }
                
                if let data = data {
                    
                    do {
                        let models_base = try JSONDecoder().decode(ModelsBase.self, from: data)
                        let models = models_base.results ?? []
                        observer.onNext(models)
                    } catch  {
                        observer.onError(error)
                    }
                }
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
        
    }
    
    
}
