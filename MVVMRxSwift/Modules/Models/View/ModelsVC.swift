//
//  ModelsVC.swift
//  MVVMRxSwift
//
//  Created by a7med on 28/02/2022.
//

import UIKit
import RxCocoa
import RxSwift


class ModelsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: ModelsListViewModel!

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUI()
        viewModel.fetchModelViewModels().bind(to: tableView.rx.items(cellIdentifier: "\(MakeCell.self)", cellType:MakeCell.self )){ index,viewModel,cell in
            cell.nameLbl.text = viewModel.displayText
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(MakeViewModel.self).subscribe(onNext: { [weak self] (makeViewModel) in
            guard let self = self else{return}
            self.viewModel.didSelectModel(makeViewModel)
        }, onError: .none, onCompleted: .none, onDisposed: .none).disposed(by: disposeBag)
    }


    
    private func setUI(){
        title = viewModel.title
        let nib = UINib(nibName: "\(MakeCell.self)", bundle: .main)
        tableView.register(nib, forCellReuseIdentifier: "\(MakeCell.self)")
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.separatorColor = .lightGray
        tableView.rowHeight = 50
        tableView.tableFooterView = UIView(frame: .zero)
        
    }


    init(viewModel: ModelsListViewModel) {
        super.init(nibName: "\(ModelsVC.self)", bundle: .main)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
